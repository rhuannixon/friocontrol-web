import React from "react";
import { Navbar } from "react-bootstrap";
import fcLogo from "../../assets/fc_logo.png";
import "./style.css";

const Header = () => (
  <div>
    <Navbar
      collapseOnSelect
      expand="lg"
      bg="light"
      variant="light"
      style={{
        display: "flex",
        width: "700px",
        alignItems: "center",
        justifyContent: "center"
      }}
    >
      <Navbar.Brand href="#">
        <img src={fcLogo} id="logo" alt="logomarca" />
      </Navbar.Brand>
    </Navbar>
  </div>
);

export default Header;
