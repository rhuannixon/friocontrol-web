import React from "react";
import { Navbar, Nav, NavDropdown } from "react-bootstrap";
import fcLogo from "../../assets/fc_logo.png";
import "./style.css";
import { logout } from "./../../services/auth";

const Header = () => (
  <div>
    <Navbar collapseOnSelect expand="lg" bg="light" variant="light">
      <Navbar.Brand href="#">
        <img src={fcLogo} id="logo" alt="logomarca" />
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link href="/home">Estoque</Nav.Link>
          <Nav.Link disabled href="/servicos">
            Serviços
          </Nav.Link>
          <Nav.Link href="/emprestimo">Emprestimo Ferramentas</Nav.Link>
          <NavDropdown title="Cadastro" id="collasible-nav-dropdown">
            <NavDropdown.Item href="/produtos">Produtos</NavDropdown.Item>
            <NavDropdown.Item href="/ferramentas">Ferramentas</NavDropdown.Item>
            <NavDropdown.Item href="/funcionarios">
              Funcionários
            </NavDropdown.Item>
          </NavDropdown>
          <Nav.Link disabled href="/servicos">
            Relatórios
          </Nav.Link>
          <Nav.Link href="/" onClick={logout}>
            Sair
          </Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  </div>
);

export default Header;
