import React from "react";
import SideNav, { NavItem, NavIcon, NavText } from "@trendmicro/react-sidenav";
import { useHistory } from "react-router-dom";
import "@trendmicro/react-sidenav/dist/react-sidenav.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faClipboardList,
  faPlus,
  faTools,
  faSignOutAlt
} from "@fortawesome/free-solid-svg-icons";
import { logout } from "./../../services/auth";

const SideBar = () => {
  const history = useHistory();
  function onNavigate(path) {
    if (path === "/logout") {
      logout();
      history.push("/");
    }
    history.push(path);
  }

  return (
    <>
      <SideNav
        onSelect={selected => {
          onNavigate(selected);
        }}
        style={{ backgroundColor: "#191970" }}
      >
        <SideNav.Toggle />
        <SideNav.Nav defaultSelected={history.location.pathname}>
          <NavItem eventKey="/home">
            <NavIcon>
              <FontAwesomeIcon
                icon={faClipboardList}
                style={{ color: "white" }}
              />
            </NavIcon>
            <NavText>Estoque</NavText>
          </NavItem>
          <NavItem eventKey="/emprestimo">
            <NavIcon>
              <FontAwesomeIcon icon={faTools} style={{ color: "white" }} />
            </NavIcon>
            <NavText>Emprestimo de ferramentas</NavText>
          </NavItem>
          <NavItem eventKey="/cadastro">
            <NavIcon>
              <FontAwesomeIcon icon={faPlus} />
            </NavIcon>
            <NavText>Cadastro</NavText>
            <NavItem eventKey="produtos">
              <NavText>Produtos</NavText>
            </NavItem>
            <NavItem eventKey="funcionarios">
              <NavText>Funcionários</NavText>
            </NavItem>
            <NavItem eventKey="ferramentas">
              <NavText>Ferramentas</NavText>
            </NavItem>
          </NavItem>
          <NavItem eventKey="/logout">
            <NavIcon>
              <FontAwesomeIcon icon={faSignOutAlt} style={{ color: "white" }} />
            </NavIcon>
            <NavText>Sair</NavText>
          </NavItem>
        </SideNav.Nav>
      </SideNav>
    </>
  );
};

export default SideBar;
