import React, { Component } from "react";
import Header from "../../components/HeaderFix";
import { MDBDataTable } from "mdbreact";
import SideBar from "../SideBar";
import api from "../../services/api";
import {
  Button,
  Modal,
  Form,
  OverlayTrigger,
  Tooltip,
  Tabs,
  Tab
} from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPencilAlt } from "@fortawesome/free-solid-svg-icons";
import toaster from "toasted-notes";
import "toasted-notes/src/styles.css";

class Ferramentas extends Component {
  state = {
    tools: [],
    show: false,
    description: "",
    descriptionEdit: "",
    idEdit: 0
  };

  handleClose = () => this.setState({ show: false });
  handleShow = () => this.setState({ show: true });

  componentDidMount() {
    this.getTools();
  }
  toastError = msg => {
    toaster.notify(msg, {
      duration: 2000
    });
  };
  getTools = async e => {
    try {
      const response = await api.get("/tool/");
      this.setState({ tools: response.data });
    } catch (err) {
      this.setState({
        error: "Usuário ou senha incorretos."
      });
    }
  };

  newTool = async e => {
    e.preventDefault();
    try {
      console.log(this.state.description);
      await api.post("/tool/", {
        description: this.state.description
      });
      this.setState({ description: "" });
      this.getTools();
      this.handleClose();
    } catch (err) {}
  };

  deleteTool = async toolId => {
    try {
      await api.delete(`/tool/${toolId}`);
      this.getTools();
    } catch (err) {}
  };

  editModal = (tool, id) => {
    this.setState({ showModalEdit: true, descriptionEdit: tool, idEdit: id });
  };
  handleCloseEdit = () => {
    this.setState({ showModalEdit: false });
  };

  edit = async () => {
    try {
      let res = await api.put("/tool", {
        id: this.state.idEdit,
        description: this.state.description
      });

      if (res.data.error) {
        this.toastError(res.data.error);
      } else if (res.data.message) {
        this.handleCloseEdit();
        this.getTools();
        this.toastError("Alteração feita com sucesso!");
      }
    } catch (e) {
      this.toastError(e.message);
    }
  };
  getData = () => {
    const rows = [];
    this.state.tools.map(tool =>
      rows.push({
        description: tool.description,
        action: (
          <>
            <OverlayTrigger
              placement="right"
              delay={{ show: 100, hide: 50 }}
              overlay={<Tooltip>Editar</Tooltip>}
            >
              <Button
                variant="primary"
                onClick={e => this.editModal(tool.description, tool.id)}
              >
                <FontAwesomeIcon icon={faPencilAlt} />
              </Button>
            </OverlayTrigger>
          </>
        )
      })
    );
    const data = {
      columns: [
        {
          label: "Ferramenta",
          field: "description",
          sort: "asc",
          width: 150
        },
        {
          label: "Ação",
          field: "action",
          sort: "asc",
          width: 50
        }
      ],
      rows
    };

    return data;
  };
  render() {
    return (
      <>
        <Header />
        <SideBar defaultSelected="cadastro" />
        <div className="container">
          <div>
            <Button id="novo" className="float-right" onClick={this.handleShow}>
              Inserir Nova
            </Button>
          </div>
          <Modal show={this.state.show} onHide={this.handleClose}>
            <Modal.Header closeButton>
              <Modal.Title>Cadastrar Nova Ferramenta</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form onSubmit={this.newTool}>
                <Form.Group controlId="exampleForm.ControlInput1">
                  <Form.Label>Descrição</Form.Label>
                  <Form.Control
                    type="text"
                    onChange={e =>
                      this.setState({ description: e.target.value })
                    }
                    placeholder="Ex. Chave de fenda"
                  />
                </Form.Group>
              </Form>
            </Modal.Body>
            <Modal.Footer>
              <Button type="submit" onClick={this.newTool} variant="primary">
                Salvar
              </Button>
            </Modal.Footer>
          </Modal>

          <Modal show={this.state.showModalEdit} onHide={this.handleCloseEdit}>
            <Modal.Header closeButton>
              <Modal.Title>Editar Ferramenta</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form>
                <Form.Group controlId="exampleForm.ControlInput1">
                  <Form.Label>Descrição</Form.Label>
                  <Form.Control
                    type="text"
                    onChange={e =>
                      this.setState({ description: e.target.value })
                    }
                    placeholder="Ex. Chave de fenda"
                    defaultValue={this.state.descriptionEdit}
                  />
                </Form.Group>
              </Form>
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={this.edit} variant="primary">
                Salvar
              </Button>
            </Modal.Footer>
          </Modal>

          <br></br>
          <br></br>
          <br></br>
          <Tabs className="tabs" defaultActiveKey="ferramenta">
            <Tab eventKey="ferramenta" title="Ferramentas">
              <div
                style={{
                  WebkitBorderRadius: "10px 10px 10px 10px",
                  borderRadius: "10px 10px 10px 10px",
                  padding: 10,
                  position: "relative",
                  WebkitBoxShadow: "0 15px 10px 0 rgba(0,0.1,0,0.1)",
                  boxShadow: "0 15px 10px 0 rgba(0.1 ,0,0,0.1)"
                }}
              >
                <MDBDataTable
                  striped
                  bordered
                  small
                  data={this.getData()}
                  entriesLabel={"itens"}
                  entries={7}
                  info={false}
                  entriesOptions={[7, 15, 30]}
                  noBottomColumns
                  noRecordsFoundLabel={"Nenhum produto encontrado"}
                  paginationLabel={["Anterior", "Próxima"]}
                  searchLabel="Buscar"
                />
              </div>
            </Tab>
          </Tabs>
        </div>
      </>
    );
  }
}

export default Ferramentas;
