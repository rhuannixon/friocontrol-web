import React, { Component } from "react";
import { withRouter } from "react-router-dom";

import Logo from "../../assets/fc_logo.png";
import api from "../../services/api";
import { login } from "../../services/auth";
import { Button, Form, Image } from "react-bootstrap";
import messageError from "../../util/toast";
import { Container } from "./styles";

class Login extends Component {
  state = {
    email: "",
    password: "",
    error: ""
  };

  handleSignIn = async e => {
    e.preventDefault();
    const { email, password } = this.state;
    if (!email || !password) {
      messageError("Preencha e-mail e senha para continuar!");
    } else {
      try {
        const response = await api.post("/user/signin", {
          login: email,
          password
        });

        if (response.data.statusCode === 401) {
          messageError("Usuário ou senha inválidos");
        } else {
          login(response.data.token);
          this.props.history.push("/home");
        }
      } catch (err) {
        messageError(err.message);
        this.setState({ email: "", password: "" });
      }
    }
  };

  render() {
    return (
      <Container>
        <Form
          style={{
            WebkitBorderRadius: "10px 10px 10px 10px",
            borderRadius: "10px 10px 10px 10px",
            padding: 30,
            maxWidth: 450,
            position: "relative",
            WebkitBoxShadow: "0 30px 60px 0 rgba(0,0,0,0.3)",
            boxShadow: "0 30px 60px 0 rgba(0,0,0,0.3)"
          }}
          onSubmit={this.handleSignIn}
        >
          <Image
            src={Logo}
            alt=""
            style={{ width: 250, height: 50, marginBottom: 30 }}
          />
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Login</Form.Label>
            <Form.Control
              type="text"
              placeholder="Digite seu login"
              onChange={e => this.setState({ email: e.target.value })}
              value={this.state.email}
            />
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>Senha</Form.Label>
            <Form.Control
              type="password"
              placeholder="Senha"
              value={this.state.password}
              onChange={e => this.setState({ password: e.target.value })}
            />
          </Form.Group>
          <Button variant="primary" type="submit">
            Entrar
          </Button>
        </Form>
      </Container>
    );
  }
}

export default withRouter(Login);
