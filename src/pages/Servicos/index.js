import React , {Component} from 'react';
import Header from '../../components/Header';
import api from '../../services/api';
import {Table,Button,Modal,Form} from 'react-bootstrap';
import format from '../../util/format';


class Servicos extends Component {
    state = {
        services:[],
        show:false
    } 
    componentDidMount(){
        this.getServices();
    }

    handleClose = () => this.setState({show:false});
    handleShow = () => this.setState({show:true});

    getServices = async () => {
        try{
            let res = await api.get('/service');
            this.setState({services:res.data});
        }catch(err){

        }
    }

    render(){
        return (
        
        <>
            <Header/>
            <div className="container">
            <Button className='float-right' id="novo" onClick={this.handleShow}>Cadastrar Novo</Button>
            </div>
            <div className="container"> 
            <Modal show={this.state.show} onHide={this.handleClose}>
                  <Modal.Header closeButton>
                    <Modal.Title>Cadastrar Novo Produto</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                  <Form>
                    <Form.Group controlId="exampleForm.ControlInput1">
                      <Form.Label>Descrição</Form.Label>
                      <Form.Control type="text" onChange={e => this.setState({ description: e.target.value })} placeholder="Split 2000btus" />
                    </Form.Group>
                    <Form.Group controlId="exampleForm.ControlInput1">
                      <Form.Label>Preço</Form.Label>
                      <Form.Control type="text" onChange={e => this.setState({ price: e.target.value })} placeholder="10.0" />
                    </Form.Group>
            
                  </Form>
                </Modal.Body>
                <Modal.Footer>
                  <Button type="submit" onClick={this.newProduct} variant="primary">
                  Salvar
                  </Button>
                </Modal.Footer>
              </Modal> 
          <Table striped bordered hover responsive>
          <thead>
            <tr>
              <th>Data do serviço</th>
              <th>Ferramenta</th>
              <th>Cliente</th>
            </tr>
          </thead>
          <tbody>
          {this.state.services.map(service => (
        <tr key={service.id}>
          <td >{format.dateTime(service.updatedAt)}</td>
          <td>{service.consumer.phone}</td>
          <td>{service.consumer.name}</td>
        </tr>

          ))}
           
          </tbody>
        </Table>
        </div>
            </>

        );
    }
}

export default Servicos;