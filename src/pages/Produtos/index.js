import React, { Component } from "react";
import Header from "../../components/HeaderFix";
import { MDBDataTable } from "mdbreact";
import SideBar from "../SideBar";
import {
  Table,
  OverlayTrigger,
  Button,
  Tooltip,
  Form,
  Modal,
  DropdownButton,
  Dropdown,
  Tab,
  Tabs
} from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPencilAlt } from "@fortawesome/free-solid-svg-icons";
import api from "../../services/api";
import "./style.css";
import toaster from "toasted-notes";
import "toasted-notes/src/styles.css";

class Produtos extends Component {
  state = {
    produtos: [],
    show: false,
    categories: [],
    measures: [],
    categoryId: null,
    measureId: null,
    showIdit: false,
    categoriesEdit: [],
    measuresEdit: [],
    showModalCat: false
  };
  handleClose = () => this.setState({ show: false });
  handleCloseEdit = () => {
    this.setState({
      showEdit: false,
      descriptionEdit: null,
      priceEdit: null,
      measureIdEdit: null,
      categoryIdEdit: null
    });
    this.getProdutos();
  };
  handleShow = () => {
    this.getCategories();
    this.getMeasures();
    this.setState({ show: true });
  };

  handleCatClose = () => this.setState({ showModalCat: false });

  handleShowModalCat = () => {
    this.getCategories();
    this.setState({ showModalCat: true });
  };

  handleMeasureClose = () => this.setState({ showModalMeasure: false });

  handleShowModalMeasure = () => {
    this.setState({ showModalMeasure: true });
    this.getMeasures();
  };

  componentDidMount() {
    this.getProdutos();
  }
  getProdutos = async e => {
    try {
      const response = await api.get("/product/");
      this.setState({ produtos: response.data });
    } catch (err) {
      this.setState({
        error: "Usuário ou senha incorretos."
      });
    }
  };

  getCategories = async e => {
    try {
      const response = await api.get("/category/");
      this.setState({ categories: response.data });
    } catch (err) {
      this.setState({
        error: "Usuário ou senha incorretos."
      });
    }
  };

  getMeasures = async e => {
    try {
      const response = await api.get("/measure/");
      this.setState({ measures: response.data });
    } catch (err) {
      this.setState({
        error: "Usuário ou senha incorretos."
      });
    }
  };

  newProduct = async () => {
    try {
      let { description, price, categoryId, measureId, code } = this.state;
      let isValid = await this.validate();
      if (isValid) {
        this.toastError(isValid);
      } else {
        let res = await api.post("/product", {
          description,
          price,
          categoryId,
          measureId,
          code
        });

        if (res.data.error) {
          this.toastError(res.data.error);
        } else {
          this.getProdutos();
          this.handleClose();
        }
      }
    } catch (err) {
      this.toastError(err.message);
    }
  };

  validate = async () => {
    let { description, price, categoryId, measureId, code } = this.state;

    if (!description) {
      return "Descrição inválida.";
    }

    if (!code) {
      return "Código inválido.";
    }

    if (!price) {
      return "Preço inválido.";
    }

    if (!categoryId || categoryId < 1) {
      return "Categoria inválida.";
    }

    if (!measureId) {
      return "Medida inválida.";
    }
  };

  newMeasure = async () => {
    try {
      let { measureDescription } = this.state;

      if (!measureDescription) {
        this.toastError("Por favor preencha o campo medida.");
      } else {
        let res = await api.post("/measure", {
          description: measureDescription
        });

        if (res.data.error) {
          this.toastError(res.data.error);
        } else {
          this.getMeasures();
        }
      }
    } catch (err) {}
  };

  newCategory = async () => {
    try {
      let { categoryDescription } = this.state;

      if (!categoryDescription) {
        this.toastError("Por favor preencher o campo descrição.");
      } else {
        await api.post("/category", {
          description: categoryDescription
        });
        this.handleCatClose();
        this.getCategories();
      }
    } catch (err) {}
  };

  modalEdit = async id => {
    try {
      let product = await api.get(`/product/${id}`);

      let measures = await api.get("/measure");

      let categories = await api.get("/category");
      this.setState({
        idEdit: product.data.id,
        descriptionEdit: product.data.description,
        priceEdit: product.data.price,
        measuresEdit: measures.data,
        measureIdEdit: product.data.measureId,
        categoriesEdit: categories.data,
        categoryIdEdit: product.data.categoryId,
        showEdit: true
      });
    } catch (error) {}
  };
  toastError = msg => {
    toaster.notify(msg, {
      duration: 2000
    });
  };
  edit = async () => {
    try {
      if (this.state.idEdit < 1) {
        console.log("Id incorreto");
      }
      let result = await api.put(`/product/${this.state.idEdit}`, {
        description: this.state.descriptionEdit,
        price: this.state.priceEdit,
        measureId: this.state.measureIdEdit,
        categoryId: this.state.categoryIdEdit
      });

      if (result.data.error) {
        console.log(result.data.error);
      } else {
        this.toastError(result.data.message);
        this.handleCloseEdit();
        this.getProdutos();
      }
    } catch (error) {
      this.toastError(error.message);
    }
  };

  editarCategoria(id, description) {}

  getData = () => {
    const rows = [];
    this.state.produtos.map(product =>
      rows.push({
        description: product.description,
        code: product.code,
        price: product.price,
        category: product.Category.description,
        measure: product.Measure.description,
        action: (
          <OverlayTrigger
            placement="top"
            delay={{ show: 50, hide: 50 }}
            overlay={<Tooltip>Editar</Tooltip>}
          >
            <Button
              tooltip="Editar"
              type="submit"
              onClick={e => this.modalEdit(product.id)}
              variant="primary"
            >
              <FontAwesomeIcon icon={faPencilAlt} />
            </Button>
          </OverlayTrigger>
        )
      })
    );
    const data = {
      columns: [
        {
          label: "Descrição",
          field: "description",
          sort: "asc",
          width: 150
        },
        {
          label: "Código",
          field: "code",
          sort: "asc",
          width: 150
        },
        {
          label: "Preço",
          field: "price",
          sort: "asc",
          width: 150
        },
        {
          label: "Categoria",
          field: "category",
          sort: "asc",
          width: 150
        },
        {
          label: "Medida",
          field: "measure",
          sort: "asc",
          width: 150
        },
        {
          label: "Ação",
          field: "action",
          sort: "asc",
          width: 50
        }
      ],
      rows
    };

    return data;
  };

  render() {
    return (
      <>
        <Header />
        <SideBar defaultSelected="cadastro" />
        <div className="container">
          <div>
            <DropdownButton
              id="novo2"
              className="float-right"
              title="Cadastrar"
            >
              <Dropdown.Item eventKey="1" onClick={this.handleShow}>
                Produtos
              </Dropdown.Item>
              <Dropdown.Item eventKey="2" onClick={this.handleShowModalCat}>
                Categorias
              </Dropdown.Item>
              <Dropdown.Item eventKey="3" onClick={this.handleShowModalMeasure}>
                Medidas
              </Dropdown.Item>
            </DropdownButton>
          </div>
          <Modal show={this.state.show} onHide={this.handleClose}>
            <Modal.Header closeButton>
              <Modal.Title>Cadastrar Novo Produto</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form>
                <Form.Group controlId="exampleForm.ControlInput1">
                  <Form.Label>Descrição</Form.Label>
                  <Form.Control
                    type="text"
                    onChange={e =>
                      this.setState({ description: e.target.value })
                    }
                    placeholder="Split 2000btus"
                  />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput1">
                  <Form.Label>Código</Form.Label>
                  <Form.Control
                    type="text"
                    onChange={e => this.setState({ code: e.target.value })}
                  />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput1">
                  <Form.Label>Preço</Form.Label>
                  <Form.Control
                    type="number"
                    onChange={e => this.setState({ price: e.target.value })}
                    placeholder="10.0"
                  />
                </Form.Group>
                <Form.Group controlId="exampleForm.Categoria">
                  <Form.Label>Categoria</Form.Label>
                  <Form.Control
                    as="select"
                    onChange={e =>
                      this.setState({ categoryId: e.target.value })
                    }
                  >
                    <option key="0" value="0">
                      {" "}
                    </option>
                    {this.state.categories.map(category => (
                      <option key={category.id} value={category.id}>
                        {category.description}
                      </option>
                    ))}
                  </Form.Control>
                </Form.Group>
                <Form.Group controlId="exampleForm.Categoria">
                  <Form.Label>Medida</Form.Label>
                  <Form.Control
                    as="select"
                    onChange={e => this.setState({ measureId: e.target.value })}
                  >
                    <option key="0" value="0">
                      {" "}
                    </option>
                    {this.state.measures.map(measure => (
                      <option key={measure.id} value={measure.id}>
                        {measure.description}
                      </option>
                    ))}
                  </Form.Control>
                </Form.Group>
              </Form>
            </Modal.Body>
            <Modal.Footer>
              <Button type="submit" onClick={this.newProduct} variant="primary">
                Salvar
              </Button>
            </Modal.Footer>
          </Modal>
          <Modal
            id="modalEdit"
            show={this.state.showEdit}
            onHide={this.handleCloseEdit}
          >
            <Modal.Header closeButton>
              <Modal.Title>Editar Produto</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form>
                <Form.Group controlId="exampleForm.ControlInput1">
                  <Form.Label>Descrição</Form.Label>
                  <Form.Control
                    type="text"
                    onChange={e =>
                      this.setState({ descriptionEdit: e.target.value })
                    }
                    defaultValue={this.state.descriptionEdit}
                  />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput1">
                  <Form.Label>Preço</Form.Label>
                  <Form.Control
                    type="number"
                    onChange={e => this.setState({ priceEdit: e.target.value })}
                    placeholder="10.0"
                    defaultValue={this.state.priceEdit}
                  />
                </Form.Group>
                <Form.Group controlId="exampleForm.Categoria">
                  <Form.Label>Categoria</Form.Label>
                  <Form.Control
                    as="select"
                    onChange={e =>
                      this.setState({ categoryIdEdit: e.target.value })
                    }
                  >
                    <option key="0" value="0">
                      {" "}
                    </option>
                    {this.state.categoriesEdit.map(category => (
                      <option key={category.id} value={category.id}>
                        {category.description}
                      </option>
                    ))}
                  </Form.Control>
                </Form.Group>
                <Form.Group controlId="exampleForm.Categoria">
                  <Form.Label>Medida</Form.Label>
                  <Form.Control
                    as="select"
                    onChange={e =>
                      this.setState({ measureIdEdit: e.target.value })
                    }
                  >
                    <option key="0" value="0">
                      {" "}
                    </option>
                    {this.state.measuresEdit.map(measure => (
                      <option key={measure.id} value={measure.id}>
                        {measure.description}
                      </option>
                    ))}
                  </Form.Control>
                </Form.Group>
              </Form>
            </Modal.Body>
            <Modal.Footer>
              <Button type="submit" onClick={this.edit} variant="primary">
                Salvar
              </Button>
            </Modal.Footer>
          </Modal>

          <Modal
            show={this.state.showModalMeasure}
            onHide={this.handleMeasureClose}
          >
            <Modal.Header closeButton>
              <Modal.Title>Cadastrar Nova Medida</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form>
                <Form.Group controlId="exampleForm.ControlInput1">
                  <Form.Label>Descrição</Form.Label>
                  <Form.Control
                    type="text"
                    onChange={e =>
                      this.setState({ measureDescription: e.target.value })
                    }
                    placeholder="Adicionar nova medida (ex.: Caixa,Unidade)"
                  />
                </Form.Group>
                <Table striped bordered hover responsive>
                  <thead>
                    <tr>
                      <th>Descrição</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.measures.map(product => (
                      <tr key={product.id}>
                        <td>{product.description}</td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              </Form>
            </Modal.Body>
            <Modal.Footer>
              <Button type="submit" onClick={this.newMeasure} variant="primary">
                Salvar
              </Button>
            </Modal.Footer>
          </Modal>

          <Modal show={this.state.showModalCat} onHide={this.handleCatClose}>
            <Modal.Header closeButton>
              <Modal.Title>Cadastrar Nova Categoria</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form>
                <Form.Group controlId="exampleForm.ControlInput1">
                  <Form.Label>Descrição</Form.Label>
                  <Form.Control
                    type="text"
                    onChange={e =>
                      this.setState({ categoryDescription: e.target.value })
                    }
                    placeholder="Adicionar nova categoria"
                  />
                </Form.Group>
                <Table striped bordered hover responsive>
                  <thead>
                    <tr>
                      <th>Descrição</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.categories.map(product => (
                      <tr key={product.id}>
                        <td>{product.description}</td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              </Form>
            </Modal.Body>
            <Modal.Footer>
              <Button
                type="submit"
                onClick={this.newCategory}
                variant="primary"
              >
                Salvar
              </Button>
            </Modal.Footer>
          </Modal>
          <br></br>
          <br></br>
          <br></br>
          <Tabs className="tabs" defaultActiveKey="produtos">
            <Tab eventKey="produtos" title="Produtos">
              <div
                style={{
                  WebkitBorderRadius: "10px 10px 10px 10px",
                  borderRadius: "10px 10px 10px 10px",
                  padding: 10,
                  position: "relative",
                  WebkitBoxShadow: "0 15px 10px 0 rgba(0,0.1,0,0.1)",
                  boxShadow: "0 15px 10px 0 rgba(0.1 ,0,0,0.1)"
                }}
              >
                <MDBDataTable
                  striped
                  bordered
                  responsive={true}
                  small
                  data={this.getData()}
                  entriesLabel={"itens"}
                  entries={7}
                  info={false}
                  entriesOptions={[7, 15, 30]}
                  noBottomColumns
                  noRecordsFoundLabel={"Nenhum produto encontrado"}
                  paginationLabel={["Anterior", "Próxima"]}
                  searchLabel="Buscar"
                />
              </div>
            </Tab>
          </Tabs>
        </div>
      </>
    );
  }
}

export default Produtos;
