import React, { Component } from "react";
import Header from "../../components/HeaderFix";
import SideBar from "../SideBar";
import { MDBDataTable } from "mdbreact";
import {
  Button,
  Modal,
  Form,
  OverlayTrigger,
  Tooltip,
  Tab,
  Tabs
} from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import api from "../../services/api";
import messageError from "../../util/toast";
import handle from "../../util/errorHandler";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCartArrowDown,
  faDollarSign
} from "@fortawesome/free-solid-svg-icons";
import format from "../../util/format";
class Home extends Component {
  state = {
    products: [],
    show: false,
    showSale: false,
    productId: 0,
    price: 0,
    amount: 0,
    sales: [],
    purchases: [],
    productToPurchase: undefined,
    productIdToPurchase: 0,
    productToSale: undefined,
    productIdToSale: 0
  };

  componentDidMount() {
    this.getTools();
    this.getPurchases();
    this.getSales();
  }

  handleClose = () => {
    this.setState({ show: false, amount: 0, productId: 0, price: 0 });
  };
  handleShow = (id, description) => {
    this.setState({
      show: true,
      productToPurchase: description,
      productIdToPurchase: id
    });
    this.getProducts();
  };

  handleCloseSale = () => {
    this.setState({ showSale: false, amount: 0, productId: 0, price: 0 });
  };
  handleShowSale = (id, description, qtd) => {
    if (qtd < 1) {
      messageError("Não existe esse produto no estoque.");
    } else {
      this.setState({
        showSale: true,
        productToSale: description,
        productIdToSale: id
      });
      this.getProducts();
    }
  };

  getProducts = async () => {
    try {
      const res = await api.get("/product/stock");
      if (res.data.statusCode !== 200) {
        this.props.history.push("/");
      }
      this.setState({ products: res.data });
    } catch (err) {
      messageError(err.message);
    }
  };

  newProduct = async () => {
    try {
      let productId = this.state.productIdToPurchase;
      let purchasePrice = "" + this.state.price;
      purchasePrice.replace(",", ".");
      let qtd = this.state.amount;

      if (!qtd || qtd < 1) {
        messageError("Campo quantidade vazio.");
      } else {
        if (!purchasePrice) {
          purchasePrice = 0.0;
        }
        const res = await api.post("/product/purchase", {
          productId,
          purchasePrice,
          qtd
        });

        if (res.data.statusCode) {
          messageError(res.data.message);
        } else {
          this.handleClose();
          this.getProducts();
          this.getPurchases();
        }
      }
    } catch (err) {
      messageError(err.message);
    }
  };

  newSale = async () => {
    try {
      let productId = this.state.productIdToSale;
      let purchasePrice = this.state.price;
      let qtd = this.state.amount;

      if (!qtd || qtd < 1) {
        messageError("Campo quantidade vazio.");
      } else {
        const res = await api.post("/service", {
          consumerId: 1,
          products: [
            {
              productId,
              purchasePrice,
              qtd
            }
          ]
        });

        if (res.data.error) {
          messageError(res.data.error);
        } else {
          this.handleCloseSale();
          this.getProducts();
          this.getSales();
        }
      }
    } catch (err) {
      messageError(err.message);
    }
  };

  getTools = async e => {
    try {
      const response = await api.get("/product/stock");
      if (response.data.statusCode === 401) {
        messageError(response.data.error);
        this.props.history.push("/");
      }
      this.setState({ products: response.data });
    } catch (err) {
      messageError(err.message);
    }
  };

  getSales = async e => {
    try {
      const response = await api.get("/product/sale");
      this.setState({ sales: response.data });
    } catch (err) {
      messageError(err.message);
    }
  };

  getPurchases = async e => {
    try {
      const response = await api.get("/product/purchase");
      this.setState({ purchases: response.data });
    } catch (err) {
      messageError(err.message);
    }
  };

  getDataStock = () => {
    const rows = [];
    this.state.products.map(product =>
      rows.push({
        description: product.Product.description,
        qtd: product.qtd,
        updatedAt: format.dateTime(product.updatedAt),
        action: (
          <>
            <OverlayTrigger
              placement="top"
              delay={{ show: 50, hide: 50 }}
              overlay={<Tooltip>Compra</Tooltip>}
            >
              <Button
                tooltip="Cadastrar Compra"
                type="submit"
                onClick={e =>
                  this.handleShow(
                    product.Product.id,
                    product.Product.description
                  )
                }
                variant="primary"
                style={{ width: "40px", height: "40px" }}
              >
                <FontAwesomeIcon icon={faCartArrowDown} />
              </Button>
            </OverlayTrigger>
            <OverlayTrigger
              placement="top"
              delay={{ show: 50, hide: 50 }}
              overlay={<Tooltip>Venda</Tooltip>}
            >
              <Button
                tooltip="Devolver"
                type="submit"
                onClick={e =>
                  this.handleShowSale(
                    product.Product.id,
                    product.Product.description,
                    product.qtd
                  )
                }
                variant="success"
                style={{ marginLeft: "10px", width: "40px", height: "40px" }}
              >
                <FontAwesomeIcon icon={faDollarSign} />
              </Button>
            </OverlayTrigger>
          </>
        )
      })
    );
    const data = {
      columns: [
        {
          label: "Produto",
          field: "description",
          sort: "asc",
          width: 150
        },
        {
          label: "Quantidade",
          field: "qtd",
          sort: "asc",
          width: 270
        },
        {
          label: "Última atualização",
          field: "updatedAt",
          sort: "asc",
          width: 50
        },
        {
          label: "Ação",
          field: "action",
          sort: "asc",
          width: 50
        }
      ],
      rows
    };

    return data;
  };

  getDataEntrada = () => {
    const rows = [];
    this.state.purchases.map(product =>
      rows.push({
        description: product.Product.description,
        qtd: product.qtd,
        price: product.purchasePrice,
        date: format.dateTime(product.createdAt)
      })
    );
    const data = {
      columns: [
        {
          label: "Produto",
          field: "description",
          sort: "asc",
          width: 150
        },
        {
          label: "Quantidade",
          field: "qtd",
          sort: "asc",
          width: 270
        },
        {
          label: "Preço",
          field: "price",
          sort: "asc",
          width: 50
        },
        {
          label: "Data",
          field: "date",
          sort: "asc",
          width: 50
        }
      ],
      rows
    };

    return data;
  };

  getDataSaida = () => {
    const rows = [];
    this.state.sales.map(product =>
      rows.push({
        description: product.product.description,
        qtd: product.qtd,
        price: product.salePrice,
        date: format.dateTime(product.createdAt)
      })
    );
    const data = {
      columns: [
        {
          label: "Produto",
          field: "description",
          sort: "asc",
          width: 150
        },
        {
          label: "Quantidade",
          field: "qtd",
          sort: "asc",
          width: 270
        },
        {
          label: "Preço",
          field: "price",
          sort: "asc",
          width: 50
        },
        {
          label: "Data",
          field: "date",
          sort: "asc",
          width: 50
        }
      ],
      rows
    };

    return data;
  };
  render() {
    return (
      <>
        <Header />
        <br></br>

        <div
          className="container"
          style={{
            borderRadius: "10px 10px 10px 10px",
            WebkitBorderRadius: "10px 10px 10px 10px"
          }}
        >
          <Modal show={this.state.show} onHide={this.handleClose}>
            <Modal.Header closeButton>
              <Modal.Title>Cadastrar Compra</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form>
                <Form.Group controlId="compra_produto">
                  <Form.Label>Produto</Form.Label>
                  <Form.Control
                    type="text"
                    value={this.state.productToPurchase}
                  />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput121">
                  <Form.Label>Preço de compra</Form.Label>
                  <Form.Control
                    type="number"
                    onChange={e => this.setState({ price: e.target.value })}
                  />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput12">
                  <Form.Label>Quantidade</Form.Label>
                  <Form.Control
                    type="number"
                    min="0"
                    defaultValue="0"
                    onChange={e => this.setState({ amount: e.target.value })}
                  />
                </Form.Group>
              </Form>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="primary" onClick={this.newProduct}>
                Cadastrar
              </Button>
            </Modal.Footer>
          </Modal>
          <Modal
            id="modal2"
            show={this.state.showSale}
            onHide={this.handleCloseSale}
          >
            <Modal.Header closeButton>
              <Modal.Title>Cadastrar Venda</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form>
                <Form.Group controlId="exampleForm.ControlInput11">
                  <Form.Label>Produto</Form.Label>
                  <Form.Control type="text" value={this.state.productToSale} />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput11">
                  <Form.Label>Preço de compra</Form.Label>
                  <Form.Control
                    type="text"
                    onChange={e => this.setState({ price: e.target.value })}
                  />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput112">
                  <Form.Label>Quantidade</Form.Label>
                  <Form.Control
                    type="number"
                    min="0"
                    defaultValue="0"
                    onChange={e => this.setState({ amount: e.target.value })}
                  />
                </Form.Group>
              </Form>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="primary" onClick={this.newSale}>
                Cadastrar
              </Button>
            </Modal.Footer>
          </Modal>
          <Tabs className="tabs" defaultActiveKey="estoque">
            <Tab eventKey="estoque" title="Estoque">
              <div
                className="container"
                style={{
                  marginTop: 10,
                  WebkitBorderRadius: "10px 10px 10px 10px",
                  borderRadius: "10px 10px 10px 10px",
                  padding: 10,
                  position: "relative",
                  WebkitBoxShadow: "0 15px 10px 0 rgba(0,0.1,0,0.1)",
                  boxShadow: "0 15px 10px 0 rgba(0.1 ,0,0,0.1)"
                }}
              >
                <MDBDataTable
                  striped
                  bordered
                  small
                  data={this.getDataStock()}
                  entriesLabel={"itens"}
                  entries={5}
                  info={false}
                  entriesOptions={[5, 10, 15]}
                  noBottomColumns
                  noRecordsFoundLabel={"Nenhum produto encontrado"}
                  paginationLabel={["Anterior", "Próxima"]}
                  searchLabel="Buscar"
                  responsive={true}
                />
              </div>
            </Tab>
            <Tab eventKey="entradas" title="Entradas">
              <div
                className="container"
                style={{
                  marginTop: 10,
                  WebkitBorderRadius: "10px 10px 10px 10px",
                  borderRadius: "10px 10px 10px 10px",
                  padding: 10,
                  position: "relative",
                  WebkitBoxShadow: "0 15px 10px 0 rgba(0,0.1,0,0.1)",
                  boxShadow: "0 15px 10px 0 rgba(0.1 ,0,0,0.1)"
                }}
              >
                <MDBDataTable
                  striped
                  bordered
                  small
                  data={this.getDataEntrada()}
                  entriesLabel={"itens"}
                  entries={7}
                  info={false}
                  entriesOptions={[7, 15, 30]}
                  noBottomColumns
                  noRecordsFoundLabel={"Nenhum produto encontrado"}
                  paginationLabel={["Anterior", "Próxima"]}
                  searchLabel="Buscar"
                />
              </div>
            </Tab>
            <Tab eventKey="saidas" title="Saídas">
              <div
                className="container"
                style={{
                  marginTop: 10,
                  WebkitBorderRadius: "10px 10px 10px 10px",
                  borderRadius: "10px 10px 10px 10px",
                  padding: 10,
                  position: "relative",
                  WebkitBoxShadow: "0 15px 10px 0 rgba(0,0.1,0,0.1)",
                  boxShadow: "0 15px 10px 0 rgba(0.1 ,0,0,0.1)"
                }}
              >
                <MDBDataTable
                  striped
                  bordered
                  small
                  data={this.getDataSaida()}
                  entriesLabel={"itens"}
                  entries={7}
                  info={false}
                  entriesOptions={[7, 15, 30]}
                  noBottomColumns
                  noRecordsFoundLabel={"Nenhum produto encontrado"}
                  paginationLabel={["Anterior", "Próxima"]}
                  searchLabel="Buscar"
                />
              </div>
            </Tab>
          </Tabs>
        </div>
        <SideBar defaultSelected="home" props={this.props} />
      </>
    );
  }
}

export default Home;
