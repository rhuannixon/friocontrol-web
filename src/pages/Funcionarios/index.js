import React, { Component } from "react";
import Header from "../../components/HeaderFix";
import { MDBDataTable } from "mdbreact";
import SideBar from "../SideBar";
import api from "../../services/api";
import toaster from "toasted-notes";
import "toasted-notes/src/styles.css";
import {
  OverlayTrigger,
  Tooltip,
  Button,
  Modal,
  Form,
  Tabs,
  Tab
} from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPencilAlt } from "@fortawesome/free-solid-svg-icons";
import "./style.css";

class Funcionarios extends Component {
  state = {
    employees: [],
    show: false,
    showEdit: false,
    toastShow: false,
    toastMsg: "",
    name: "",
    bDate: null,
    registration: null,
    phone: null,
    nameEdit: null,
    idEdit: null
  };

  handleClose = () => this.setState({ show: false });
  handleShow = () => this.setState({ show: true });
  handleCloseEdit = () => this.setState({ showEdit: false });
  handleShowEdit = (name, id) =>
    this.setState({ showEdit: true, nameEdit: name, idEdit: id });

  componentDidMount() {
    this.getFunc();
  }

  getFunc = async e => {
    try {
      const response = await api.get("/employee/");
      this.setState({ employees: response.data });
    } catch (err) {
      this.setState({
        error: "Usuário ou senha incorretos."
      });
    }
  };

  newFunc = async () => {
    try {
      let res = await api.post("/employee", {
        name: this.state.name,
        occupationId: 1
      });
      if (res.data.error) {
        this.toastError(res.data.error);
      } else {
        this.handleClose();
        this.setState({ name: null });
        this.getFunc();
      }
    } catch (err) {
      console.log(err.message);
      //this.setState({toastMsg:err});
      //this.toast();
    }
  };

  toast = () => this.setState({ toastShow: true });

  toastError = msg => {
    toaster.notify(msg, {
      duration: 2000
    });
  };

  edit = async () => {
    try {
      let res = await api.put("/employee", {
        id: this.state.idEdit,
        name: this.state.nameEdit
      });

      if (res.data.error) {
        this.toastError(res.data.error);
      } else if (res.data.message) {
        this.toastError(res.data.message);
        this.handleCloseEdit();
        this.getFunc();
      }
    } catch (e) {
      this.toastError(e.message);
    }
  };
  getData = () => {
    const rows = [];
    this.state.employees.map(func =>
      rows.push({
        name: func.name,
        action: (
          <OverlayTrigger
            placement="right"
            delay={{ show: 150, hide: 50 }}
            overlay={<Tooltip>Editar</Tooltip>}
          >
            <Button
              onClick={e => this.handleShowEdit(func.name, func.id)}
              variant="primary"
            >
              <FontAwesomeIcon icon={faPencilAlt} />
            </Button>
          </OverlayTrigger>
        )
      })
    );
    const data = {
      columns: [
        {
          label: "Nome",
          field: "name",
          sort: "asc",
          width: 150
        },
        {
          label: "Ação",
          field: "action",
          sort: "asc",
          width: 50
        }
      ],
      rows
    };

    return data;
  };
  render() {
    return (
      <>
        <Header />
        <SideBar defaultSelected="cadastro" />
        <div className="container">
          <Button id="novo" className="float-right" onClick={this.handleShow}>
            Cadastrar Novo
          </Button>
          <Modal show={this.state.show} onHide={this.handleClose}>
            <Modal.Header closeButton>
              <Modal.Title>Cadastrar Novo Funcionário</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form>
                <Form.Group controlId="exampleForm.ControlInput1">
                  <Form.Label>Nome</Form.Label>
                  <Form.Control
                    type="text"
                    onChange={e => this.setState({ name: e.target.value })}
                    placeholder="João Carlos"
                  />
                </Form.Group>
              </Form>
            </Modal.Body>
            <Modal.Footer>
              <Button type="submit" onClick={this.newFunc} variant="primary">
                Salvar
              </Button>
            </Modal.Footer>
          </Modal>

          <Modal show={this.state.showEdit} onHide={this.handleCloseEdit}>
            <Modal.Header closeButton>
              <Modal.Title>Editar Funcionário</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form>
                <Form.Group controlId="exampleForm.ControlInput1">
                  <Form.Label>Nome</Form.Label>
                  <Form.Control
                    type="text"
                    onChange={e => this.setState({ nameEdit: e.target.value })}
                    placeholder="João Carlos"
                    defaultValue={this.state.nameEdit}
                  />
                </Form.Group>
              </Form>
            </Modal.Body>
            <Modal.Footer>
              <Button type="submit" onClick={this.edit} variant="primary">
                Salvar
              </Button>
            </Modal.Footer>
          </Modal>

          <br></br>
          <br></br>
          <br></br>
          <Tabs className="tabs" defaultActiveKey="funcionario">
            <Tab eventKey="funcionario" title="Funcionários">
              <div
                style={{
                  WebkitBorderRadius: "10px 10px 10px 10px",
                  borderRadius: "10px 10px 10px 10px",
                  padding: 10,
                  position: "relative",
                  WebkitBoxShadow: "0 15px 10px 0 rgba(0,0.1,0,0.1)",
                  boxShadow: "0 15px 10px 0 rgba(0.1 ,0,0,0.1)"
                }}
              >
                <MDBDataTable
                  striped
                  bordered
                  small
                  data={this.getData()}
                  entriesLabel={"itens"}
                  entries={7}
                  info={false}
                  entriesOptions={[7, 15, 30]}
                  noBottomColumns
                  noRecordsFoundLabel={"Nenhum funcionário encontrado"}
                  paginationLabel={["Anterior", "Próxima"]}
                  searchLabel="Buscar"
                />
              </div>
            </Tab>
          </Tabs>
        </div>
      </>
    );
  }
}

export default Funcionarios;
