import React, { Component } from "react";
import Header from "../../components/HeaderFix";
import { MDBDataTable } from "mdbreact";
import SideBar from "../SideBar";
import api from "../../services/api";
import {
  Button,
  Modal,
  Form,
  Tooltip,
  OverlayTrigger,
  Tabs,
  Tab
} from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheckSquare } from "@fortawesome/free-solid-svg-icons";
import format from "../../util/format";

class EmprestimoFerramentas extends Component {
  state = {
    loans: [],
    show: false,
    setShow: false,
    toolId: "",
    employeeId: "",
    availableTools: [],
    employees: [],
    loanId: 0,
    loanHistoric: []
  };

  componentDidMount() {
    this.getLoans();
    this.availableTools();
    this.loanHistoric();
  }

  handleClose = () => {
    this.setState({ show: false });
  };
  handleShow = () => {
    this.availableTools();
    this.getEmployees();
    this.setState({ show: true });
  };

  loanHistoric = async () => {
    try {
      const response = await api.get("/tool/loan");
      this.setState({ loanHistoric: response.data });
    } catch (err) {
      this.setState({
        error: "Usuário ou senha incorretos."
      });
    }
  };

  getLoans = async e => {
    try {
      const response = await api.get("/tool/loan/list");
      this.setState({ loans: response.data });
    } catch (err) {
      this.setState({
        error: "Usuário ou senha incorretos."
      });
    }
  };

  devolution = async loanId => {
    try {
      await api.post(`/tool/loan/devolution/${loanId}`);
      this.getLoans();
      this.loanHistoric();
      this.availableTools();
    } catch (err) {
      this.setState({
        erro: "Não foi possível devolver este item."
      });
    }
  };

  loan = async () => {
    try {
      await api.post("/tool/loan/");
    } catch (err) {}
  };

  availableTools = async () => {
    try {
      const res = await api.get("/tool/available");
      this.setState({ availableTools: res.data });
    } catch (err) {}
  };

  getEmployees = async () => {
    try {
      const res = await api.get("/employee/");
      this.setState({ employees: res.data });
    } catch (err) {}
  };

  newLoan = async () => {
    try {
      const { toolId, employeeId } = this.state;

      if (toolId > 0 && employeeId > 0) {
        const res = await api.post("/tool/loan", { toolId, employeeId });
        console.log(res.data);
      } else {
        console.log("Parametros inválidos.");
      }
      this.handleClose();
      this.getLoans();
      this.loanHistoric();
      this.availableTools();
    } catch (err) {
      console.log(err);
    }
  };
  getDataEmprestimo = () => {
    const rows = [];
    this.state.loans.map(loan =>
      rows.push({
        date: format.dateTime(loan.createdAt),
        ferramenta: loan.Tool.description,
        employee: loan.Employee.name,
        action: (
          <OverlayTrigger
            placement="top"
            delay={{ show: 150, hide: 50 }}
            overlay={<Tooltip>Devolver</Tooltip>}
          >
            <Button
              tooltip="Devolver"
              type="submit"
              onClick={e => this.devolution(loan.id)}
              variant="primary"
            >
              <FontAwesomeIcon icon={faCheckSquare} />
            </Button>
          </OverlayTrigger>
        )
      })
    );
    const data = {
      columns: [
        {
          label: "Data Empréstimo",
          field: "date",
          sort: "asc",
          width: 150
        },
        {
          label: "Ferramenta",
          field: "ferramenta",
          sort: "asc",
          width: 150
        },
        {
          label: "Funcionário",
          field: "employee",
          sort: "asc",
          width: 150
        },
        {
          label: "Ação",
          field: "action",
          sort: "asc",
          width: 150
        }
      ],
      rows
    };

    return data;
  };

  getDataDisponiveis = () => {
    const rows = [];
    this.state.availableTools.map(loan =>
      rows.push({
        description: loan.description,
        action: (
          <OverlayTrigger
            placement="top"
            delay={{ show: 50, hide: 50 }}
            overlay={<Tooltip>Emprestar</Tooltip>}
          >
            <Button
              tooltip="Emprestar"
              type="submit"
              onClick={e => this.devolution(loan.id)}
              variant="primary"
            >
              <FontAwesomeIcon icon={faCheckSquare} />
            </Button>
          </OverlayTrigger>
        )
      })
    );
    const data = {
      columns: [
        {
          label: "Ferramenta",
          field: "description",
          sort: "asc",
          width: 150
        },
        {
          label: "Ação",
          field: "action",
          sort: "asc",
          width: 150
        }
      ],
      rows
    };

    return data;
  };

  getDataHistorico = () => {
    const rows = [];
    this.state.loanHistoric.map(loan =>
      rows.push({
        date: format.dateTime(loan.createdAt),
        ferramenta: loan.Tool.description,
        employee: loan.Employee.name,
        devolution:
          format.dateTime(loan.createdAt) === format.dateTime(loan.updatedAt)
            ? "Pendente"
            : format.dateTime(loan.updatedAt)
      })
    );
    const data = {
      columns: [
        {
          label: "Data de Emprestimo",
          field: "date",
          sort: "asc",
          width: 150
        },
        {
          label: "Ferramenta",
          field: "ferramenta",
          sort: "asc",
          width: 150
        },
        {
          label: "Funcionário",
          field: "employee",
          sort: "asc",
          width: 150
        },
        {
          label: "Data da devolução",
          field: "devolution",
          sort: "asc",
          width: 150
        }
      ],
      rows
    };

    return data;
  };
  render() {
    return (
      <>
        <Header />
        <div className="container">
          <Button className="float-right" id="novo" onClick={this.handleShow}>
            Inserir Nova
          </Button>
        </div>
        <SideBar defaultSelected="emprestimo" />
        <div className="container">
          <Modal show={this.state.show} onHide={this.handleClose}>
            <Modal.Header closeButton>
              <Modal.Title>Cadastrar Emprestimo</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form>
                <Form.Group controlId="exampleForm.ControlSelect1">
                  <Form.Label>Ferramentas</Form.Label>
                  <Form.Control
                    as="select"
                    onChange={e => this.setState({ toolId: e.target.value })}
                  >
                    <option key="0" value="0">
                      {" "}
                    </option>
                    {this.state.availableTools.map(tool => (
                      <option key={tool.id} value={tool.id}>
                        {tool.description}
                      </option>
                    ))}
                  </Form.Control>
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlSelect1">
                  <Form.Label>Funcionários</Form.Label>
                  <Form.Control
                    as="select"
                    onChange={e =>
                      this.setState({ employeeId: e.target.value })
                    }
                  >
                    <option key="0" value="0">
                      {" "}
                    </option>
                    {this.state.employees.map(employee => (
                      <option key={employee.id} value={employee.id}>
                        {employee.name}
                      </option>
                    ))}
                  </Form.Control>
                </Form.Group>
              </Form>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="primary" onClick={this.newLoan}>
                Cadastrar
              </Button>
            </Modal.Footer>
          </Modal>
          <Tabs id="tabs1" defaultActiveKey="emprestimos">
            <Tab eventKey="emprestimos" title="Empréstimos">
              <div
                style={{
                  WebkitBorderRadius: "10px 10px 10px 10px",
                  borderRadius: "10px 10px 10px 10px",
                  padding: 10,
                  position: "relative",
                  WebkitBoxShadow: "0 15px 10px 0 rgba(0,0.1,0,0.1)",
                  boxShadow: "0 15px 10px 0 rgba(0.1 ,0,0,0.1)"
                }}
              >
                <MDBDataTable
                  striped
                  bordered
                  responsive={true}
                  small
                  data={this.getDataEmprestimo()}
                  entriesLabel={"itens"}
                  entries={7}
                  info={false}
                  entriesOptions={[7, 15, 30]}
                  noBottomColumns
                  noRecordsFoundLabel={"Nenhuma ferramenta emprestada"}
                  paginationLabel={["Anterior", "Próxima"]}
                  searchLabel="Buscar"
                />
              </div>
            </Tab>
            <Tab eventKey="disponiveis" title="Disponíveis">
              <div
                style={{
                  WebkitBorderRadius: "10px 10px 10px 10px",
                  borderRadius: "10px 10px 10px 10px",
                  padding: 10,
                  position: "relative",
                  WebkitBoxShadow: "0 15px 10px 0 rgba(0,0.1,0,0.1)",
                  boxShadow: "0 15px 10px 0 rgba(0.1 ,0,0,0.1)"
                }}
              >
                <MDBDataTable
                  striped
                  bordered
                  responsive={true}
                  small
                  data={this.getDataDisponiveis()}
                  entriesLabel={"itens"}
                  entries={7}
                  info={false}
                  entriesOptions={[7, 15, 30]}
                  noBottomColumns
                  noRecordsFoundLabel={"Nenhuma ferramenta disponível"}
                  paginationLabel={["Anterior", "Próxima"]}
                  searchLabel="Buscar"
                />
              </div>
            </Tab>
            <Tab eventKey="historico" title="Histórico">
              <div
                style={{
                  WebkitBorderRadius: "10px 10px 10px 10px",
                  borderRadius: "10px 10px 10px 10px",
                  padding: 10,
                  position: "relative",
                  WebkitBoxShadow: "0 15px 10px 0 rgba(0,0.1,0,0.1)",
                  boxShadow: "0 15px 10px 0 rgba(0.1 ,0,0,0.1)"
                }}
              >
                <MDBDataTable
                  striped
                  bordered
                  responsive={true}
                  small
                  data={this.getDataHistorico()}
                  displayEntries={false}
                  entries={10}
                  info={false}
                  noBottomColumns
                  noRecordsFoundLabel={"Nenhuma ferramenta no histórico"}
                  paginationLabel={["Anterior", "Próxima"]}
                  searching={false}
                />
              </div>
            </Tab>
          </Tabs>
        </div>
      </>
    );
  }
}

export default EmprestimoFerramentas;
