import React from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import Login from "./pages/Login";
import Home from "./pages/Home";
import { isAuthenticated } from "./services/auth";
import Ferramentas from "./pages/Ferramentas";
import Servicos from "./pages/Servicos";
import Emprestimo from "./pages/EmprestimoFerramentas";
import Produtos from "./pages/Produtos";
import Funcionarios from "./pages/Funcionarios";

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      isAuthenticated() ? (
        <Component {...props} />
      ) : (
          <Redirect to={{ pathname: "/", state: { from: props.location } }} />
        )
    }
  />
);

const Routes = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Login} />
        <PrivateRoute path="/Home" component={Home} />
        <PrivateRoute path="/ferramentas" component={Ferramentas} />
        <PrivateRoute path="/servicos" component={Servicos} />
        <PrivateRoute path="/emprestimo" component={Emprestimo} />
        <PrivateRoute path="/produtos" component={Produtos} />
        <PrivateRoute path="/funcionarios" component={Funcionarios} />
        <PrivateRoute path="/logout" component={Login} />
        <PrivateRoute path="*" component={() => <h1>Page not found</h1>} />
      </Switch>
    </BrowserRouter>
  );
};

export default Routes;
