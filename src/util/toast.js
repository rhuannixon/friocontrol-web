import toaster from "toasted-notes";
import "toasted-notes/src/styles.css";

const messageError = msg => {
  toaster.notify(msg, {
    duration: 2000
  });
};

export default messageError;
