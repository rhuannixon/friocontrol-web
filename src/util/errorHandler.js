const messageError = require("./toast");

const handle = (props, response) => {
  if (response.statusCode === 401) {
    messageError(response.message);
    props.history.push("/");
  }
};

export default handle;
