exports.dateTime = function(date) {
    var datetime = new Date(date);

    return datetime.toLocaleDateString() + ' ' + datetime.toLocaleTimeString('en-GB');
}